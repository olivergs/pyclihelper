# Python CLI command development helper library

## Command definition

The command definition is a dictionary with the arguments and base configuration for the command.

This data will define the arguments the command can take and the default behavior for common
situations.

This is a sample command definition:

```python
command_definition = {
    "name": "testcommand",
    "desc": "Command description",
    "userconfig": True, # User configuration read from .config/{name}/defaults.yml
    "args": {
        # Basic positional argument
        "mydir": {
            "desc": "My directory",
            # "type": str (default type of arguments is str)
        },
        # Optional positional parameter (arguments with default: None are optional)
        "myoptionaldir": {
            "desc": "My optional directory",
            "default": None
        },
        # Option arguments have shortcuts
        "name": {
            "shortcut": "n",
            "desc": "Name for the output"
        },
        # Verbose switch (option arguments with type bool are considered switches)
        "verbose": {
            "shortcut": "v",
            "desc": "Show more messages",
            "type": bool,
            "default": False,
        }
    }
}
```