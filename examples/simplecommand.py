#!/usr/bin/python3
"""Test command."""

from pyclihelper.command import BaseCommand


class MyCommand(BaseCommand):
    """Test command."""
    
    command_definition = {
        "name": "testcommand",
        "desc": "Command description",
        "userconfig": True, # User configuration read from .config/{name}/defaults.yml
        "args": {
            # Basic positional argument
            "mydir": {
                "desc": "My directory",
                # "type": str (default type of arguments is str)
            },
            # Optional positional parameter (arguments with default: None are optional)
            "myoptionaldir": {
                "desc": "My optional directory",
                "default": None
            },
            # Option arguments have shortcuts
            "name": {
                "shortcut": "n",
                "desc": "Name for the output"
            },
            # Verbose switch (option arguments with type bool are considered switches)
            "verbose": {
                "shortcut": "v",
                "desc": "Show more messages",
                "type": bool,
                "default": False,
            }
        }
    }

    def run_command(self, args):
        """Run command."""
        if args.name:
            print(f"Using output name {args.name}")
        else:
            print(f"Using default output")
        if args.myoptionaldir:
            print(f"Optional directory set to {args.myoptionaldir}")
        else:
            print("No optional directory specified")
        if args.verbose:
            print(f"Executing verbose on directory {args.mydir}!")
        else:
            print(f"Executing quiet on directory {args.mydir}!")


if __name__ == "__main__":
    cmd = MyCommand()
    cmd.run()