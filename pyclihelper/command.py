"""Command module."""

import argparse
import os
import sys

import yaml


class BaseCommand:
    """Base command class."""

    command_definition = {}

    def __init__(self):
        """Command initialization method."""
        if self.command_definition.get("userconfig", False):
            self.defaults = self.read_defaults()
        else:
            self.defaults = {}
        self.setup_argument_parser()

    def setup_argument_parser(self):
        """Set up command argument parser using command definition data."""
        self.argument_parser = argparse.ArgumentParser(
            description=self.command_definition.get("desc", "")
        )

        for argname, argdef in self.command_definition.get("args", {}).items():
            argopts = {
                "help": argdef.get("desc", argname.title),
            }
            # Define default value by precedence
            if argname in self.defaults:
                default = self.defaults[argname]
            else:
                default = argdef.get("default", None)
            argtype = argdef.get("type", str)
            if argtype == bool:
                # If the argument type is a boolean we assume it is a switch
                if default is not None:
                    if default:
                        argopts["action"] = "store_false"
                    else:
                        argopts["action"] = "store_true"
            else:
                argopts["metavar"] = argname.upper()
                argopts["type"] = argtype
                if "default" in argdef:
                    # If the default value is None, the argument is optional
                    if argdef["default"] is None:
                        argopts["nargs"] = "?"
                argopts["default"] = default

            if "shortcut" in argdef:
                # If we have a shortcut we assume it is an option
                argnames = [
                    f"-{argdef['shortcut']}",
                    f"--{argname}",
                ]
            else:
                argnames = [
                    argname,
                ]

            self.argument_parser.add_argument(*argnames, **argopts)

    def read_defaults(self):
        """Load defaults from user configuration directory."""
        defaults = {}
        command_name = self.command_definition["name"]
        config_dir = os.path.expanduser(f"~/.config/{command_name}")
        config_file = os.path.join(config_dir, "defaults.yml")
        if os.path.isfile(config_file):
            with open(config_file, "r", encoding="utf-8") as filepointer:
                defaults = yaml.safe_load(filepointer)
        return defaults

    def run(self):
        """Run command."""
        args = self.argument_parser.parse_args(sys.argv[1:])
        self.run_command(args)

    def run_command(self, args):
        """Run command."""
        raise NotImplementedError()

    def exit(self, exitcode, message=None):
        """Exit command with given exit code and message."""
        if message:
            if exitcode == 0:
                print(message)
            else:
                print(message, file=sys.stderr)
